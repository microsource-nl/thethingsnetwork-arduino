/***************************************************************************
 Send GPS, MPY9650 and BMP260 (GY-91) data by LoRa connection to The Things Network

 Sodaq Explorer board
 
 Sketch Version: 20190123

 Used libraries:
  - Wire - standard Arduino core distribution
  - TinyGPS++ - https://github.com/mikalhart/TinyGPSPlus /  http://arduiniana.org/
  - MPU9250 - https://github.com/bolderflight/MPU9250
  - Adafruit_Sensor - https://github.com/adafruit/Adafruit_Sensor
  - Adafruit_BMP280 - https://github.com/adafruit/Adafruit_BMP280_Library
  - TheThingsNetwork - https://github.com/TheThingsNetwork/arduino-device-lib

 ***************************************************************************/

// Choose transmission mode
//#define PLAINDATA
#define CAYENNE

// Comment out one of these two defines (choose join method)
#define OTAA
//#define ABP

//+++++++++++++++++++++++++++
// I2C bus driver
#include <Wire.h>

// Serial port over USB on Sodaq Explorer board
#define debugSerial SerialUSB

//+++++++++++++++++++++++++++
// MPU-9250 sensor on I2C bus 0 with address 0x68
#include "MPU9250.h"
MPU9250 IMU(Wire, 0x68);
int status;
float zero_height_pressure = 1002.25;

//+++++++++++++++++++++++++++
// BMP280 using I2C
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>
Adafruit_BMP280 bme;

//+++++++++++++++++++++++++++
// LoRa
#include <TheThingsNetwork.h>

#ifdef CAYENNE
  // For Cayenne
  #include <CayenneLPP.h>
  CayenneLPP lpp(51); 
#endif

#ifdef OTAA
  // FOR OTAA - Set your AppEUI and AppKey
  const char *appEui = "CHANGE_ME";
  const char *appKey = "CHANGE_ME";
#endif

#ifdef ABP
  // FOR ABP
  // if we have session keys we can join with Activation By Personalization (ABP)
  // devAddr - Device Address found on the deviceâs page in the console.
  const char *devAddr = "CHANGE_ME";
  // nwkSKey - the Network Session Key.
  const char *nwkSKey = "CHANGE_ME";
  // appSKey - App Session Key.
  const char *appSKey = "CHANGE_ME";
#endif

#define loraSerial Serial2
#define freqPlan TTN_FP_EU868

TheThingsNetwork ttn(loraSerial, debugSerial, freqPlan);

#ifdef PLAINDATA
  byte payload[30];
#endif

uint8_t led = 0;

//+++++++++++++++++++++++++++
// For timing
const long interval = 60000;
unsigned long start;

//+++++++++++++++++++++++++++
// The TinyGPS++ library and object
#include <TinyGPS++.h>
TinyGPSPlus gps;

//+++++++++++++++++++++++++++
void setup() {

  Serial.begin(9600);
  loraSerial.begin(57600);
  debugSerial.begin(9600);
  delay(1000);

  // Configure the Sodaq eplorer button as an input and enable the internal pull-up resistor
  pinMode(BUTTON, INPUT_PULLUP);
  pinMode(LED_BUILTIN, OUTPUT);
  
  //define Sodaq eplorer temperature pin as input
  pinMode(TEMP_SENSOR, INPUT);
  
  // Wait a maximum of 30s for Serial Monitor
  while (!debugSerial && millis() < interval)
    ;
 
  // start communication with IMU 
  status = IMU.begin();
  if (status < 0) {
    debugSerial.println("IMU initialization unsuccessful");
    debugSerial.println("Check IMU wiring or try cycling power");
    debugSerial.print("Status: ");
    debugSerial.println(status);
    //while(1) {}
  }
  // setting the accelerometer full scale range to +/-8G 
  IMU.setAccelRange(MPU9250::ACCEL_RANGE_8G);
  // setting the gyroscope full scale range to +/-500 deg/s
  IMU.setGyroRange(MPU9250::GYRO_RANGE_500DPS);
  // setting DLPF bandwidth to 20 Hz
  IMU.setDlpfBandwidth(MPU9250::DLPF_BANDWIDTH_20HZ);
  // setting SRD to 19 for a 50 Hz update rate
  IMU.setSrd(19);

  if (!bme.begin())
  {  
    debugSerial.println("Could not find a valid BMP280 sensor, check wiring!");
    //while (1);
  }
   
   debugSerial.print("TinyGPS++ libray version:  ");
   debugSerial.println(gps.libraryVersion());

  // LoRa 
  #ifdef OTAA
    debugSerial.println("-- JOIN (OTAA)");  
    ttn.join(appEui, appKey);
  #endif
  #ifdef ABP
    debugSerial.println("-- PERSONALIZE (ABP)");
    ttn.personalize(devAddr, nwkSKey, appSKey);
  #endif
  
  debugSerial.println("-- STATUS");
  ttn.showStatus();
  
  // Set callback for incoming messages
  ttn.onMessage(message);
  pinMode(LED_BUILTIN, OUTPUT);
  
  start = millis();
}

//+++++++++++++++++++++++++++
void loop() {
  debugSerial.println("-- LOOP");    
  while (millis() - start < interval);
  // set start now -> every round same timing
  start = millis();
  getData();
  
  #ifdef PLAINDATA
    ttn.sendBytes(payload, sizeof(payload));
  #endif
  #ifdef CAYENNE
    ttn.sendBytes(lpp.getBuffer(), lpp.getSize());
  #endif

}

//+++++++++++++++++++++++++++
static void getData() {

  // read GPS data
  smartDelay(1000);
 
  float f_latitude = gps.location.lat();
  float f_longitude= gps.location.lng();
  float f_altitude  = gps.altitude.meters();
  uint32_t latitude = (uint32_t)(f_latitude * 1000000);
  uint32_t longitude = (uint32_t)(f_longitude * 1000000);
       
  debugSerial.print("lat: ");
  debugSerial.print(f_latitude);
  debugSerial.print("  -  ");
  debugSerial.print(latitude);
  debugSerial.print("  long: ");
  debugSerial.print(f_longitude);     
  debugSerial.print("  -  ");
  debugSerial.print(longitude);     
  debugSerial.print("  alt: ");
  debugSerial.println(f_altitude);     
    
  //+++++++++++++++++++++++++++
  // read the MPU9250 sensor
  IMU.readSensor();
  
  float accel_x = IMU.getAccelX_mss();
  float accel_y = IMU.getAccelY_mss();
  float accel_z = IMU.getAccelZ_mss();
  
  float gyr_x = IMU.getGyroX_rads();
  float gyr_y = IMU.getGyroY_rads();
  float gyr_z = IMU.getGyroZ_rads();
  
  float mag_x = IMU.getMagX_uT();
  float mag_y = IMU.getMagY_uT();
  float mag_z = IMU.getMagZ_uT();
  
  float temperature = bme.readTemperature();
  float pressure = bme.readPressure();

  //Sodaq temp sensor - 10mV per C, 0C is 500mV
  float mVolts = (float)analogRead(TEMP_SENSOR) * 3300.0 / 1023.0;
  float sodaq_temp = (mVolts - 500.0) / 10.0;
   
  int buttonVal = digitalRead(BUTTON);
  if (buttonVal == LOW)
  {
    zero_height_pressure = pressure;
    debugSerial.print("Reset zero altitude:  ");
    debugSerial.println(zero_height_pressure);
    digitalWrite(LED_BUILTIN, LOW);        
    } else {
      digitalWrite(LED_BUILTIN, HIGH);
  }
  float altitude = bme.readAltitude(zero_height_pressure);

  debugSerial.print(temperature);
  debugSerial.print("\t");
  debugSerial.println(pressure);
  
  debugSerial.print(accel_x);
  debugSerial.print("\t");
  debugSerial.print(accel_y);
  debugSerial.print("\t");
  debugSerial.println(accel_z);
  
  debugSerial.print(gyr_x);
  debugSerial.print("\t");
  debugSerial.print(gyr_y);
  debugSerial.print("\t");
  debugSerial.println(gyr_z);     
  
  debugSerial.print(mag_x);
  debugSerial.print("\t");
  debugSerial.print(mag_y);
  debugSerial.print("\t");
  debugSerial.println(mag_z);  

  #ifdef PLAINDATA
    // Create payload
    payload[0] = latitude >> 24;
    payload[1] = latitude >> 16;
    payload[2] = latitude >> 8;
    payload[3] = latitude;
    
    payload[4] = longitude >> 24;
    payload[5] = longitude >> 16;
    payload[6] = longitude >> 8;
    payload[7] = longitude;  
    
    int temp = temperature * 10;
    payload[8] = highByte(temp);
    payload[9] = lowByte(temp);
    
    int pres = pressure / 10;
    payload[10] = highByte(pres);
    payload[11] = lowByte(pres);
    
    // acceleration
    word i_acc_x = 32000 + (accel_x * 100);
    payload[12] = highByte(i_acc_x);
    payload[13] = lowByte(i_acc_x);
    
    word i_acc_y = 32000 + (accel_y * 100);
    payload[14] = highByte(i_acc_y);
    payload[15] = lowByte(i_acc_y);
    
    word i_acc_z = 32000 + (accel_z * 100);
    payload[16] = highByte(i_acc_z);
    payload[17] = lowByte(i_acc_z);
    
    // gyroscope
    word i_gyr_x = 32000 + (gyr_x * 100);
    payload[18] = highByte(i_gyr_x);
    payload[19] = lowByte(i_gyr_x);
    
    word i_gyr_y = 32000 + (gyr_y * 100);
    payload[20] = highByte(i_gyr_y);
    payload[21] = lowByte(i_gyr_y);
    
    word i_gyr_z = 32000 + (gyr_z * 100);
    payload[22] = highByte(i_gyr_z);
    payload[23] = lowByte(i_gyr_z);

    // heading
    word i_mag_x = 32000 + (mag_x * 100);
    payload[24] = highByte(i_mag_x);
    payload[25] = lowByte(i_mag_x);
    
    word i_mag_y = 32000 + (mag_y * 100);
    payload[26] = highByte(i_mag_y);
    payload[27] = lowByte(i_mag_y);
    
    word i_mag_z = 32000 + (mag_z * 100);
    payload[28] = highByte(i_mag_z);
    payload[29] = lowByte(i_mag_z);
  #endif
  
  #ifdef CAYENNE
    lpp.reset();
    lpp.addTemperature(1, temperature);
    lpp.addTemperature(2, sodaq_temp);
    lpp.addBarometricPressure(3, pressure / 100);
    lpp.addGyrometer(4, gyr_x, gyr_y, gyr_z);
    lpp.addAccelerometer(5, accel_x, accel_y, accel_z);
    lpp.addGPS(6, f_latitude, f_longitude, f_altitude);      
    // LPP is missing compass data mode      
  #endif
}

//+++++++++++++++++++++++++++
void message(const byte *payload, size_t size, port_t port){
  debugSerial.println("-- MESSAGE");

  if (size != 4 && payload[0] != 2 && payload[1] != 00) {
    debugSerial.println("Order not supported");
  }
  if (payload[2] > 0) {
    debugSerial.println("LED ON");
    led = 1;
    digitalWrite(LED_BUILTIN, HIGH);
  } else {
    debugSerial.println("LED OFF");
    led = 0;
    digitalWrite(LED_BUILTIN, LOW);
  }
}

// This custom version of delay() ensures that the gps object is being "fed".
static void smartDelay(unsigned long ms)
{
  unsigned long start = millis();
  do 
  {
    while (Serial.available())
      gps.encode(Serial.read());
  } while (millis() - start < ms);
}

///////////////////////////////////////////////////
// The Things Network payload format for plain data
//
//function Decoder(bytes, port) {
//  var lat = ((bytes[0] << 24) | (bytes[1] << 16) | (bytes[2] << 8) | bytes[3]) / 1000000;
//  var lon = ((bytes[4] << 24) | (bytes[5] << 16) | (bytes[6] << 8) | bytes[6]) / 1000000;
//  var temperature = ((bytes[8] << 8) | bytes[9]) / 10;
//  var pressure = ((bytes[10] << 8) | bytes[11]) / 10;
//  var accel_x = (((bytes[12] << 8) | bytes[13]) - 32000) / 100;
//  var accel_y = (((bytes[14] << 8) | bytes[15]) - 32000) / 100;
//  var accel_z = (((bytes[16] << 8) | bytes[17]) - 32000) / 100;
//  var gyr_x = (((bytes[18] << 8) | bytes[19]) - 32000) / 100;
//  var gyr_y = (((bytes[20] << 8) | bytes[21]) - 32000) / 100;
//  var gyr_z = (((bytes[22] << 8) | bytes[23]) - 32000) / 100;
//  var mag_x = (((bytes[24] << 8) | bytes[25]) - 32000) / 100;
//  var mag_y = (((bytes[26] << 8) | bytes[27]) - 32000) / 100;
//  var mag_z = (((bytes[28] << 8) | bytes[29]) - 32000) / 100;
//
//  return {
//    lat: lat,
//    lon: lon,
//    temperature: temperature,
//    pressure: pressure,
//    accel_x: accel_x,
//    accel_y: accel_y,
//    accel_z: accel_z,
//    gyr_x: gyr_x,
//    gyr_y: gyr_y,
//    gyr_z: gyr_z,
//    mag_x: mag_x,
//    mag_y: mag_y,
//    mag_z: mag_z,
//  }
//}
