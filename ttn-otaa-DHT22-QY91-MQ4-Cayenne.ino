/*******************************************************************************
 * 
 *  Send DHT-22, QY-91 and MQ-4 data by LoRa wireless connection to The Things Network
 *  LoRaWAN Academy week 4 Assignment: Weather Station
 *  
 *  Arduino Mega 2560 + Dragino LoRa Shield
 *  Using MCCI_LoRaWAN_LMIC_library
 * 
 *  Version 2019-01-23
 * 
 * Based on:
 * Copyright (c) 2015 Thomas Telkamp and Matthijs Kooijman
 * Copyright (c) 2018 Terry Moore, MCCI
 * 
 *  MQ-4
 *  Calculate R0 value with speical sketch
 *  https://www.geekstips.com/mq4-sensor-natural-gas-methane-arduino/
 *   
*******************************************************************************/
/////////////////////////////////////
// Set the following defines to what you wish to  do

// Set this to false to test the sensors without transmitting data with LoRa
const boolean send_lora = true;

// Choose one of these two join options by commenting out unused define
//#define ABP
#define OTAA

// Define CAYENNE data format to send data to Cayenne (comment it out to send simple data format)
#define CAYENNE

// Use Cayenne  TTN LPP library to encode data
//#define LPPLIB

// Use all available EU channels
#define CFG_eu868

///////////////////////////////////////////
// For timing
// every ten minutes: 600000
const long interval = 600000;

/////////////////////////////////////
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>


// OTAA
#ifdef OTAA
  // The EUI must be in little-endian format, so least-significant-byte first. 
  // When copying an EUI from ttnctl output, this means to reverse the bytes. 
  static const u1_t PROGMEM DEVEUI[8]={ CHANGE_ME };
  void os_getDevEui (u1_t* buf) { memcpy_P(buf, DEVEUI, 8);}
  
  //The APPEUI EUI must be in little-endian format. For TTN issued APPEUIs the last bytes should be 0xD5, 0xB3, 0x70.
  static const u1_t PROGMEM APPEUI[8]={ CHANGE_ME };
  void os_getArtEui (u1_t* buf) { memcpy_P(buf, APPEUI, 8);}
  
  // This key should be in big endian format (or, since it is not really a  number but a block of memory, endianness does not really apply). 
  // In practice, a key taken from ttnctl can be copied as-is.
  static const u1_t PROGMEM APPKEY[16] = { CHANGE_ME };
  void os_getDevKey (u1_t* buf) {  memcpy_P(buf, APPKEY, 16);}

#endif
#ifdef ABP
  //////////////////////////////////////////////
  // ABP
  
  // LoRaWAN NwkSKey, network session key
  static const PROGMEM u1_t NWKSKEY[16] = { CHANGE_ME6 };
  
  // LoRaWAN AppSKey, application session key
  static const u1_t PROGMEM APPSKEY[16] = { CHANGE_ME };
  
  // LoRaWAN end-device address (DevAddr)
  // See http://thethingsnetwork.org/wiki/AddressSpace
  // The library converts the address to network byte order as needed.
  // Example: static const u4_t DEVADDR = 0xfceb3e8f ;
  static const u4_t DEVADDR = 0xCHANGE_ME ; // <-- Change this address for every node! (hex)
  
  // These callbacks are only used in over-the-air activation, so they are
  // left empty here (we cannot leave them out completely unless
  // DISABLE_JOIN is set in arduino-lmic/project_config/lmic_project_config.h,
  // otherwise the linker will complain).
  void os_getArtEui (u1_t* buf) { }
  void os_getDevEui (u1_t* buf) { }
  void os_getDevKey (u1_t* buf) { }
#endif

static osjob_t sendjob;

// Schedule TX every this many seconds (might become longer due to duty cycle limitations).
const unsigned TX_INTERVAL = 60;

// Pin mapping for Arduino Dragino LoRa Shield
const lmic_pinmap lmic_pins = {
    .nss = 10,
    .rxtx = LMIC_UNUSED_PIN,
    .rst = 9,
    .dio = {2, 6, 7},
};

///////////////////////////////////////////
// DHT-22
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#define DHTPIN  4  // Pin which is connected to the DHT sensor.
#define DHTTYPE DHT22 // DHT 22 (AM2302)
DHT_Unified dht(DHTPIN, DHTTYPE);
float temp;
float humid;
uint32_t delayMS;

///////////////////////////////////////////
const int gasPin = 0; //GAS sensor output pin to Arduino analog A0 pin
int gas_value;

// For calculating ppm
//https://www.geekstips.com/mq4-sensor-natural-gas-methane-arduino/
float m = -0.318; //Slope
float b = 1.133; //Y-Intercept
float sensor_volt; //Define variable for sensor voltage
float RS_gas; //Define variable for sensor resistance
float ratio; //Define variable for ratio
float sensorValue ; // analog value of sensor
// Run measuring code to set zero before 
float R0 = 33; //Sensor Resistance in fresh air from measuring code

//+++++++++++++++++++++++++++
// MPU-9250 sensor on I2C bus 0 with address 0x68
#include "MPU9250.h"
MPU9250 IMU(Wire, 0x68);
int status;
// this is arbitrary since it depends on the weather
float zero_height_pressure = 1002.25;

//+++++++++++++++++++++++++++
// BMP280 using I2C
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>
Adafruit_BMP280 bme;

///////////////////////////////////////////
#ifdef CAYENNE
  #ifdef LPPLIB
    #include <CayenneLPP.h> 
    CayenneLPP lpp(51); 
  #else
    static uint8_t mydata[24];
  #endif
  uint8_t led = 0;
#else
  static uint8_t mydata[12];
#endif

unsigned long start;

///////////////////////////////////////////
void setup() {
  Serial.begin(9600);
  Serial.println(F("Starting"));

  // Initialize DHT-22 device.
  dht.begin();
  Serial.println("DHTxx Unified Sensor Initialisation");
  sensor_t sensor;
  dht.temperature().getSensor(&sensor);
  dht.humidity().getSensor(&sensor);
  // Set delay between sensor readings based on sensor details.
  delayMS = sensor.min_delay / 1000;
  Serial.println("------------------------------------");
  Serial.print  ("Sensor:       "); Serial.println(sensor.name);
  Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
  Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
  Serial.println("Temperature");
  Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println(" *C");
  Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println(" *C");
  Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println(" *C");  
  Serial.println("Humidity");
  Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println("%");
  Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println("%");
  Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println("%");  
  Serial.println("------------------------------------");
  
  // start communication with IMU 
  status = IMU.begin();
  if (status < 0) {
    Serial.println("IMU initialization unsuccessful");
    Serial.println("Check IMU wiring or try cycling power");
    Serial.print("Status: ");
    Serial.println(status);
    //while(1) {}
  }
  // setting the accelerometer full scale range to +/-8G 
  IMU.setAccelRange(MPU9250::ACCEL_RANGE_8G);
  // setting the gyroscope full scale range to +/-500 deg/s
  IMU.setGyroRange(MPU9250::GYRO_RANGE_500DPS);
  // setting DLPF bandwidth to 20 Hz
  IMU.setDlpfBandwidth(MPU9250::DLPF_BANDWIDTH_20HZ);
  // setting SRD to 19 for a 50 Hz update rate
  IMU.setSrd(19);
  
  //////////////////////////////////
  if (!bme.begin())
  {  
    Serial.println("Could not find a valid BMP280 sensor, check wiring!");
    //while (1);
  }
  float bme_temperature = bme.readTemperature();
  float bme_pressure = bme.readPressure();
  Serial.print("BMP-280\tTemperature: ");
  Serial.print(bme_temperature);
  Serial.print("\t\tPressure: ");
  Serial.println(bme_pressure / 100, 6);
  // set zero height to current pressure
  zero_height_pressure = bme_pressure / 100;
  Serial.println("------------------------------------");
  
  // Configure Input pin for MQ-4 Gas Senosr
  pinMode(gasPin, INPUT); //Set gas sensor as input

  if (send_lora == true) {
    Serial.println("Init LoRa connection");
    // LMIC init
    os_init();
    // Reset the MAC state. Session and pending data transfers will be discarded.
    LMIC_reset();

  ///////////////////
  #ifdef ABP
    // For ABP
    // Set static session parameters. Instead of dynamically establishing a session
    // by joining the network, precomputed session parameters are be provided.
    #ifdef PROGMEM
      // On AVR, these values are stored in flash and only copied to RAM
      // once. Copy them to a temporary buffer here, LMIC_setSession will
      // copy them into a buffer of its own again.
      uint8_t appskey[sizeof(APPSKEY)];
      uint8_t nwkskey[sizeof(NWKSKEY)];
      memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
      memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
      LMIC_setSession (0x13, DEVADDR, nwkskey, appskey);
    #else
      // If not running an AVR with PROGMEM, just use the arrays directly
      LMIC_setSession (0x13, DEVADDR, NWKSKEY, APPSKEY);
    #endif

    #if defined(CFG_eu868)
      // Set up the channels used by the Things Network, which corresponds
      // to the defaults of most gateways. Without this, only three base
      // channels from the LoRaWAN specification are used, which certainly
      // works, so it is good for debugging, but can overload those
      // frequencies, so be sure to configure the full frequency range of
      // your network here (unless your network autoconfigures them).
      // Setting up channels should happen after LMIC_setSession, as that
      // configures the minimal channel set.
      LMIC_setupChannel(0, 868100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
      LMIC_setupChannel(1, 868300000, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI);      // g-band
      LMIC_setupChannel(2, 868500000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
      LMIC_setupChannel(3, 867100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
      LMIC_setupChannel(4, 867300000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
      LMIC_setupChannel(5, 867500000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
      LMIC_setupChannel(6, 867700000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
      LMIC_setupChannel(7, 867900000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
      LMIC_setupChannel(8, 868800000, DR_RANGE_MAP(DR_FSK,  DR_FSK),  BAND_MILLI);      // g2-band
      // TTN defines an additional channel at 869.525Mhz using SF9 for class B
      // devices' ping slots. LMIC does not have an easy way to define set this
      // frequency and support for class B is spotty and untested, so this
      // frequency is not configured here.
      #elif defined(CFG_us915)
      // NA-US channels 0-71 are configured automatically
      // but only one group of 8 should (a subband) should be active
      // TTN recommends the second sub band, 1 in a zero based count.
      // https://github.com/TheThingsNetwork/gateway-conf/blob/master/US-global_conf.json
      LMIC_selectSubBand(1);
    #endif

    // Disable link check validation
    LMIC_setLinkCheckMode(0);

    // TTN uses SF9 for its RX2 window.
    LMIC.dn2Dr = DR_SF9;

    // Set data rate and transmit power for uplink (note: txpow seems to be ignored by the library)
    LMIC_setDrTxpow(DR_SF7,14);
    
  // End ABP
  #endif
  ///////////////////////
  
  // Start job (sending automatically starts OTAA too)
  do_send(&sendjob);
  }

  start = millis();
  Serial.println(F("Finished setup"));
}

void loop() {
//  Serial.println("Sending...");
//  do_send(&sendjob, mydata);
//  //Run LMIC loop until he as finish
//  while(flag_TXCOMPLETE == 0)
//  {
    os_runloop_once();
//  }
//  flag_TXCOMPLETE = 0;
}

void onEvent (ev_t ev) {
    Serial.print(os_getTime());
    Serial.print(": ");
    switch(ev) {
        case EV_SCAN_TIMEOUT:
            Serial.println(F("EV_SCAN_TIMEOUT"));
            break;
        case EV_BEACON_FOUND:
            Serial.println(F("EV_BEACON_FOUND"));
            break;
        case EV_BEACON_MISSED:
            Serial.println(F("EV_BEACON_MISSED"));
            break;
        case EV_BEACON_TRACKED:
            Serial.println(F("EV_BEACON_TRACKED"));
            break;
        case EV_JOINING:
            Serial.println(F("EV_JOINING"));
            break;
        case EV_JOINED:
            Serial.println(F("EV_JOINED"));
            {
              u4_t netid = 0;
              devaddr_t devaddr = 0;
              u1_t nwkKey[16];
              u1_t artKey[16];
              LMIC_getSessionKeys(&netid, &devaddr, nwkKey, artKey);
              Serial.print("netid: ");
              Serial.println(netid, DEC);
              Serial.print("devaddr: ");
              Serial.println(devaddr, HEX);
              Serial.print("artKey: ");
              for (int i=0; i<sizeof(artKey); ++i) {
                Serial.print(artKey[i], HEX);
              }
              Serial.println("");
              Serial.print("nwkKey: ");
              for (int i=0; i<sizeof(nwkKey); ++i) {
                Serial.print(nwkKey[i], HEX);
              }
              Serial.println("");
            }
            // Disable link check validation (automatically enabled during join,
            // but because slow data rates change max TX size, we don't use it in this example.
            LMIC_setLinkCheckMode(0);
            break;
        /*
        || This event is defined but not used in the code. No
        || point in wasting codespace on it.
        ||
        || case EV_RFU1:
        ||     Serial.println(F("EV_RFU1"));
        ||     break;
        */
        case EV_JOIN_FAILED:
            Serial.println(F("EV_JOIN_FAILED"));
            break;
        case EV_REJOIN_FAILED:
            Serial.println(F("EV_REJOIN_FAILED"));
            break;
        case EV_TXCOMPLETE:
            Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
            if (LMIC.txrxFlags & TXRX_ACK)
              Serial.println(F("Received ack"));
            if (LMIC.dataLen) {
              Serial.print(F("Received "));
              Serial.print(LMIC.dataLen);
              Serial.println(F(" bytes of payload"));
            }
            // Schedule next transmission
            os_setTimedCallback(&sendjob, os_getTime()+sec2osticks(TX_INTERVAL), do_send);
            break;
        case EV_LOST_TSYNC:
            Serial.println(F("EV_LOST_TSYNC"));
            break;
        case EV_RESET:
            Serial.println(F("EV_RESET"));
            break;
        case EV_RXCOMPLETE:
            // data received in ping slot
            Serial.println(F("EV_RXCOMPLETE"));
            break;
        case EV_LINK_DEAD:
            Serial.println(F("EV_LINK_DEAD"));
            break;
        case EV_LINK_ALIVE:
            Serial.println(F("EV_LINK_ALIVE"));
            break;
        /*
        || This event is defined but not used in the code. No
        || point in wasting codespace on it.
        ||
        || case EV_SCAN_FOUND:
        ||    Serial.println(F("EV_SCAN_FOUND"));
        ||    break;
        */
        case EV_TXSTART:
            Serial.println(F("EV_TXSTART"));
            break;
        default:
            Serial.print(F("Unknown event: "));
            Serial.println((unsigned) ev);
            break;
    }
}

void do_send(osjob_t* j){

   getData();

    // Check if there is not a current TX/RX job running
    if (LMIC.opmode & OP_TXRXPEND) {
        Serial.println(F("OP_TXRXPEND, not sending"));
    } else {
        // Prepare upstream data transmission at the next possible time.
        #ifdef CAYENNE
          #ifdef LPPLIB
              LMIC_setTxData2(1, lpp.getBuffer(), lpp.getSize(), 0);
          #else
              LMIC_setTxData2(1, mydata, sizeof(mydata)-1, 0);
          #endif
        #else
          LMIC_setTxData2(1, mydata, sizeof(mydata)-1, 0);
        #endif
        
        Serial.println(F("Packet queued"));
    }
    // Next TX is scheduled after TX_COMPLETE event.
}

void getData(){

  //Minimal Delay between measurements for DHT-22.
  delay(delayMS);
  while (millis() - start < interval);
  // set start now -> every round same timing
  start = millis();

  Serial.println("Get Data ------------------------------------");

  ////////////////////////////////////////////////
  // Read the MQ-4 Gas Sensor
  float sensorValue = analogRead(gasPin); //Read analog values of sensor
  Serial.print("MQ-4\tAnalog val: "); 
  Serial.print(sensorValue); 

  sensor_volt = sensorValue * (5.0 / 1023.0); //Convert analog values to voltage
  RS_gas = ((5.0 * 10.0) / sensor_volt) - 10.0; //Get value of RS in a gas
  ratio = RS_gas / R0;   // Get ratio RS_gas/RS_air

  double ppm_log = (log10(ratio) - b) / m; //Get ppm value in linear scale according to the the ratio value
  double ppm = pow(10, ppm_log); //Convert ppm value to log scale
  double percentage = ppm / 10000; //Convert to percentage
  Serial.print("\t\tppm: "); 
  Serial.println(percentage); 

  ////////////////////////////////////////////////
  // Get DHT-22 temperature event and print its value.
  sensors_event_t event;  
  dht.temperature().getEvent(&event);
  if (isnan(event.temperature)) {
    Serial.println("Error reading temperature!");
  }
  else {
    temp = event.temperature;    
    Serial.print("DHT-22\tTemperature: ");
    Serial.print(temp);
    Serial.print(" *C    ");
  }
  // Get humidity event and print its value.
  dht.humidity().getEvent(&event);
  if (isnan(event.relative_humidity)) {
    Serial.println("Error reading humidity!");
  }
  else { 
    humid = event.relative_humidity;    
    Serial.print("\tHumidity: ");
    Serial.print(humid);
    Serial.println("%");
  }

  ////////////////////////////////////////////////
  // read the MPU9250 sensor
  IMU.readSensor();
  
  float accel_x = IMU.getAccelX_mss();
  float accel_y = IMU.getAccelY_mss();
  float accel_z = IMU.getAccelZ_mss();
  
  float gyr_x = IMU.getGyroX_rads();
  float gyr_y = IMU.getGyroY_rads();
  float gyr_z = IMU.getGyroZ_rads();
  
  float mag_x = IMU.getMagX_uT();
  float mag_y = IMU.getMagY_uT();
  float mag_z = IMU.getMagZ_uT();
  
  ////////////////////////////////////////////////
  // read the BMP280 sensor
  float bme_temperature = bme.readTemperature();
  float bme_pressure = bme.readPressure();

  Serial.print("BMP-280\tTemperature: ");
  Serial.print(bme_temperature);
  Serial.print("\t\tPressure: ");
  Serial.println(bme_pressure / 100, 6);

  ////////////////////////////////////////////////
  // Convert data for LoRa transmission
  #ifndef CAYENNE
    // standard format
    // DHT-22
    int16_t  i_temp = (int) (temp * 10);
    mydata[0] = highByte(i_temp);
    mydata[1] = lowByte(i_temp);
    int16_t  i_humid = (int) (humid * 10);
    mydata[2] = highByte(i_humid);
    mydata[3] = lowByte(i_humid);
    
    // MQ-4 gas sensor
    int16_t i_sensorValue = (int) (sensorValue * 100);
    mydata[4] = highByte(i_sensorValue);
    mydata[5] = lowByte(i_sensorValue);
    int16_t i_percentage = (int) (percentage * 10);
    mydata[6] = highByte(i_percentage);
    mydata[7] = lowByte(i_percentage); 
  
    // BMP-280
    int16_t i_bme_pressure= (int) (bme_pressure * 10);
    mydata[8] = highByte(i_bme_pressure);
    mydata[9] = lowByte(i_bme_pressure);
    int16_t i_bme_temperature = (int) (bme_temperature * 100);
    mydata[10] = highByte(i_bme_temperature);
    mydata[11] = lowByte(i_bme_temperature);
  #endif

  #ifdef CAYENNE
    #ifdef LPPLIB
      lpp.reset();
      lpp.addTemperature(1, temp);
      lpp.addRelativeHumidity(2,humid);
      lpp.addAnalogInput(3, sensorValue);
      lpp.addAnalogInput(4, percentage);       
      lpp.addBarometricPressure(5, bme_pressure / 100);
      lpp.addTemperature(6, bme_temperature);
    #else 
      /////////////////////////////////////////////
      uint8_t cursor = 0;
      //Cayenne LPP format
      // Data Types conform to the IPSO Alliance Smart Objects Guidelines, 
      // which identifies each data type with an âObject IDâ. 
      // However a conversion is made to fit the Object ID into a single byte:
      // LPP_DATA_TYPE = IPSO_OBJECT_ID - 3200
      
      // DHT-22
      // one bye data channel
      mydata[cursor++] = 0x01;  // 1
      // one byte data type
      // Type                 IPSO  LPP   Hex   Data Size   Data Resolution per bit
      // Temperature Sensor   3303  103   67    2           0.1 Â°C Signed MSB
      mydata[cursor++] = 0x67;  // 2
      int i_temp = (int) (temp * 10);
      mydata[cursor++] = highByte(i_temp);  // 3
      mydata[cursor++] = lowByte(i_temp);   // 4
      
      // Cayenne humidity
      // one bye data channel
      mydata[cursor++] = 0x02;  //5
      // Type              IPSO  LPP   Hex   Data Size   Data Resolution per bit
      // Humidity Sensor   3304  104   68    1           0.5 % Unsigned
      mydata[cursor++] = 0x68; // 6
      unsigned int  i_humid = (unsigned int) (humid * 2);
      mydata[cursor++] = lowByte(i_humid); // 7
      
      // MQ-4 gas sensor
      // one bye data channel
      mydata[cursor++] = 0x03; // 8
      // analog voltage
      // Type           IPSO  LPP   Hex   Data Size   Data Resolution per bit
      // Analog Input   3202  2     2     2           0.01 Signed
      mydata[cursor++] = 0x02; // 9
      int i_sensorValue = (int) (sensorValue * 100);
      mydata[cursor++] = highByte(i_sensorValue);  // 10
      mydata[cursor++] = lowByte(i_sensorValue);   // 11
  
      // ppm
      // one bye data channel
      mydata[cursor++] = 0x04;  // 12
      mydata[cursor++] = 0x02;  // 13  
      int i_percentage = (int) (percentage * 100);
      mydata[cursor++] = highByte(i_percentage); // 14
      mydata[cursor++] = lowByte(i_percentage);  // 15
      
      // BMP-280
      // one bye data channel
      mydata[cursor++] = 0x05;  // 16
      // Type           IPSO  LPP   Hex   Data Size   Data Resolution per bit
      // Barometer      3315  115   73    2           0.1 hPa Unsigned MSB
      mydata[cursor++] = 0x73;  // 17
      unsigned int i_bme_pressure= (unsigned int) (bme_pressure / 10);
      mydata[cursor++] = highByte(i_bme_pressure); // 18
      mydata[cursor++] = lowByte(i_bme_pressure);  // 19
      
      // Temperature
      // one bye data channel
      mydata[cursor++] = 0x06; // 20
      // Type                 IPSO  LPP   Hex   Data Size   Data Resolution per bit
      // Temperature Sensor   3303  103   67    2           0.1 Â°C Signed MSB
      mydata[cursor++] = 0x67; // 21
      int i_bme_temperature = (int) (bme_temperature * 10);
      mydata[cursor++] = highByte(i_bme_temperature); // 22
      mydata[cursor++] = lowByte(i_bme_temperature);  // 23
    #endif
  #endif
}
